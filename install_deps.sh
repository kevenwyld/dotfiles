#!/bin/bash
set -x

pacman --noconfirm -S \
       tree \
       neovim \
       vim \
       exfat-utils \
       fish \
       adobe-source-code-pro-fonts \
       xfce4-screenshooter \
       i3-wm \
       i3lock \
       i3status \
       lightdm \
       lightdm-gtk-greeter \
       gnome-themes-extra \
       rofi \
       rxvt-unicode \
       keepassxc \
       icu \
       qt5-base \
       json-c \
       git \
       xorg-xbacklight \
       rxvt-unicode \
       firefox \
       autocutsel \
       clipmenu \
       redshift
