#!/bin/bash

dir=$HOME/git/dotfiles
olddir=$HOME/.dotfiles_old
files=".config/dunst .config/fish .config/nvim .config/redshift.conf .config/keepassxc .i3 .urxvt .vimrc .xsession .Xdefaults .Xresources .gnupg/gpg-agent.conf .config/qt5ct .profile .gtkrc-2.0 .config/gtk-3.0"

echo "Creating $olddir for existing dotfiles in $HOME..."
mkdir $olddir
echo "...done"

echo "Moving old files to $olddir..."
for file in $files; do
  mv -f $HOME/$file $olddir/
done
echo "...Done"

#Make some directores that we know we need
mkdir $HOME/.config 2>/dev/null
mkdir $HOME/.gnupg  2>/dev/null
mkdir $HOME/bin  2>/dev/null

for file in $files; do
  echo "Copying $file to $HOME"
  cp -r $dir/$file $HOME/$file
done
