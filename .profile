# For HIDPI:
# export QT_FONT_DPI=140
export QT_QPA_PLATFORMTHEME="qt5ct"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
gpg-connect-agent updatestartuptty /bye 2>&1 > /dev/null

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
