" execute pathogen#infect()

syntax on
set hlsearch
set shell=bash
set expandtab
set tabstop=2
set incsearch

map <F2> :set tabstop=2<CR>
map <F3> !}fmt<CR>
map <F4> :set tabstop=4<CR>
map <F8> :set tabstop=8<CR>
map <F5> :set number!<CR>

set background=dark
