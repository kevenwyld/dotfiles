#!/bin/bash
set -x

if [[ $(whoami) == "root" ]]; then
  echo "do not run as root"
  exit 1
fi

wget -P "$HOME/" "https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-29.noarch.rpm"
sudo dnf -y install $HOME/rpmfusion*.rpm
sudo dnf -y copr enable skidnik/clipmenu
sudo dnf -y copr enable shymega/autocutsel-copr
sudo dnf -y install \
  vim \
  exfat-utils \
  fish \
  adobe-source-code-pro-fonts \
  xfce4-screenshooter \
  i3 \
  i3lock \
  rxvt-unicode-256color \
  keepassxc \
  util-linux-user \
  redshift \
  clipmenu \
  clipnotify \
  rofi \
  rofi-themes \
  autocutsel

setxkbmap -option caps:super
xset r rate 200 40
chsh liveuser -s /usr/bin/fish
gdbus call --system --dest org.freedesktop.Accounts --object-path /org/freedesktop/Accounts/User"$(id -u)" --method org.freedesktop.Accounts.User.SetXSession "i3"
#mount /run/initramfs/live -o remount,size=1T
#mount /run/archiso/cowspace -o remount,size=1T
