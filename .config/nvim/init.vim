syntax on
set hlsearch
set shell=bash
set expandtab
set tabstop=2
set shiftwidth=0
set incsearch
set background=dark
set mouse=""
set inccommand=nosplit

map <F2> :set tabstop=2<CR>
map <F3> !}fmt<CR>
map <F4> :set tabstop=4<CR>
map <F8> :set tabstop=8<CR>
map <F5> :set number!<CR>
