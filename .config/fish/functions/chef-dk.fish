function chef-dk --description 'Initialize current shell with chefy stuff'
   eval (chef shell-init fish); and cd ~/git/chef
   setenv KITCHEN_LOG_OVERWRITE 'false'
end
