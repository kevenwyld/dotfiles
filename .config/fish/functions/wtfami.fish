function wtfami
  if set -q argv[1]
    readlink -f $argv
  else
    readlink -f .
  end
end
