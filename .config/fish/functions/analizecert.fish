function analizecert
  curl --insecure -v https://$argv[1] 2>&1 | awk 'BEGIN { cert=0 } /^\* Server certificate:/ { cert=1 } /^\*/ { if (cert) print }'
end
