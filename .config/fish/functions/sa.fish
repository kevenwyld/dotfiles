function sa --description 'show git status on all directories in pwd'
	find . -mindepth 1 -maxdepth 1 -type d |parallel "echo {} && set_color red; git -C (pwd)/{} status --short; set_color normal"
end
