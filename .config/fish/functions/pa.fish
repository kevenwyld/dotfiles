function pa --description 'attempt git pull on all directories in pwd'
  find . -mindepth 1 -maxdepth 1 -type d |parallel "git -C (pwd)/{} pull"
end
