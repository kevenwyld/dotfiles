function rwomi
  set IFS ';'
  msf |sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mK]//g" |sed -r -n -e 's:^\s*([0-9]+)[+ ][- ].*\*\* *(PROBLEM|RECOVERY) *alert *- *([^/]+)/(.+) *is *(WARNING|CRITICAL|OK|UNKNOWN) *\*\*$:\1;\3;\4:p' |sed -e 's/ \+/ /g'|\
  while read number hostname service_name
    set check_status (echo "GET services\nFilter: host_name = $hostname\nFilter: service_description = $service_name\nColumns: state" |ssh countdown /usr/local/livestatus/bin/unixcat /data/livestatus/var/rw/live)
    [[ $check_status == "0" ]]; and echo $number
  end
  set -e IFS
end
