function garbdock -d "run garburator with docker on the specified host"
  printf "\npulling garburator repo\n"
  fish -c 'cd ~/git/garburator-run/garburator; and git pull'

  printf "\npulling nagios repo\n"
  fish -c 'cd ~/git/garburator-run/nagios_config_unix; and git pull'

  printf "\npulling systems list repo\n"
  fish -c 'cd ~/git/garburator-run/systems; and git pull'

  printf "\nupdating hosts file from damona\n"
  fish -c 'scp damona:/distrib/common/netfiles/hosts ~/git/garburator-run/'

  ~/git/garburator-run/garburator/run -H $argv -g ~/git/garburator-run -c ~/git/garburator-run/hosts -d

  if test $status -eq 0
    printf "\ncding to nagios_config_unix so you can do git stuffs\n"
    cd ~/git/garburator-run/nagios_config_unix
  end
end
