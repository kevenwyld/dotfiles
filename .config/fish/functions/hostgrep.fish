function hostgrep --description "runs hostgrep alias on damona"
  ssh damona.colorado.edu "~/bin/fish -c \"hostgrep $argv\""
end
