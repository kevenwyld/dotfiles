function analize-apache -d "greps apache access logs and sorts them according to number of hits by ip"
  awk '{ print $1 }' $argv | awk -F'.' '{ print $1"."$2"."$3 }' | sort | uniq -c | sort -nr | head
end
