function snapshot -d "makes a snapshot of the / and /home subvolumes"
  echo "asking for sudo password to make snapshots"
  sudo -k
  sudo -v
  sudo btrfs subvolume snapshot /home /snapshots/snapshot_home(date -Iseconds)
  sudo btrfs subvolume snapshot / /snapshots/snapshot_slash(date -Iseconds)
end
