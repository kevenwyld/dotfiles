function getipv6
  host -t AAAA $argv |grep "has IPv6 address" |awk '{print $5}'
end
