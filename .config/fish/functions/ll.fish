function ll --description 'List contents of directory using long format, arrange by date'
	ls -lchtr $argv
end
