function setkbd -d "sets up the keyboard to your liking"
  xset r rate 200 40
  setxkbmap -option caps:super
end
