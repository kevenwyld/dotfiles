function la --description 'List contents of directory using long format, arrange by date, include hidden files'
	ls -lchtra $argv
end
