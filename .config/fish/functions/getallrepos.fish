function getallrepos --description 'pulls all repos for the chef project'
    if [ ! (pwd) = "/home/$USER/git/chef" ]
        printf "you're in the wrong directory, try ~$USER/git/chef"
    else if [ ! -x ./get_all_repos.py ]
        printf "get_all_repos.py in the current directroy isn't executable"
    else
        pass identikey/$USER |./get_all_repos.py |parallel -j 20 "
            echo {}
            if [ ! -d {} ]
                git clone ssh://git@stash.int.colorado.edu:7999/sc/{}.git
            else if [ (git -C ./{} status --porcelain |wc -c) -eq 0 ]; and [ (git -C ./{} symbolic-ref HEAD) = 'refs/heads/master' ]
                git -C ./{} pull
                git -C ./{} fetch -p
            else
                set_color red; git -C ./{} branch; set_color normal
                set_color yellow; git -C ./{} status --branch --porcelain; set_color normal
            end"
    end
end
