function gpg
  if [ -x (which gpg2) ]
    gpg2 $argv
  else
    echo 'no gpg2 found'
  end
end
