function garb -d "run garburator without web on the specified host"
  printf "\npulling garburator repo\n"
  fish -c 'cd ~/git/garburator-run/garburator; and git pull'

  printf "\npulling nagios repo\n"
  fish -c 'cd ~/git/garburator-run/monitor_unix; and git pull'

  printf "\npulling systems list repo\n"
  fish -c 'cd ~/git/garburator-run/systems; and git pull'

  printf "\nupdating hosts file from damona\n"
  fish -c 'scp damona:/distrib/common/netfiles/hosts ~/git/garburator-run/'

  ~/git/garburator-run/garburator/run -H $argv -g ~/git/garburator-run -c ~/git/garburator-run/hosts

  if test $status -eq 0
    printf "\ncding to monitor_unix so you can do git stuffs\n"
    cd ~/git/garburator-run/monitor_unix
  end
end
