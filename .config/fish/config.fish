### Environment Variables ###
setenv TERM xterm-256color
setenv BROWSER "firefox"
setenv EDITOR "/usr/bin/nvim"
set -x PATH /bin /usr/bin /sbin /usr/sbin /usr/local/bin /usr/local/sbin ~/bin

### Other Variables ###
set fish_greeting ""
set fish_color_search_match --background=black
