#!/bin/bash
set -x

pacman --noconfirm -Syy
pacman-key --refresh-keys
pacman --noconfirm -S \
       tree \
       neovim \
       exfat-utils \
       fish \
       adobe-source-code-pro-fonts \
       xfce4-screenshooter \
       i3lock \
       rxvt-unicode \
       keepassxc \
       icu \
       qt5-base \
       json-c
       
setxkbmap -option caps:super
xset r rate 200 40
chsh root -s /usr/bin/fish
mount /run/archiso/cowspace -o remount,size=1T
